// POC Code
var io = require('socket.io-client');
var colors = require('colors');

function FoosballClient() {
    /* Variables
    --------------------------------------------------- */
    var self = this;
    this.config = require('./config.json');

    /* Basic Functions
    --------------------------------------------------- */
    this.initialize = function() {
        colors.setTheme(this.config.theme);
        var hostname = 'icarus';
        console.log('initialize:'.status, 'client:'.dark, hostname);
        this.endpoint = 'http://' + this.config.server.host + ':' + this.config.server.port;
        this.connect(hostname, this.endpoint);
    };

    this.connect = function(name, endpoint) {
        console.log('connecting:'.conn, endpoint);
        this.client = io.connect(endpoint);
        this.client.on('connect', function() {
            console.log('connection:'.conn, 'successful:'.good, self.endpoint);
            self.client.emit('client.register', {
                name: name
            });
        });
        this.client.on('disconnect', function() {
            console.log('connection:'.conn, 'disconnected:'.bad, self.endpoint);
        });
        this.client.on('connect_failed', function() {
            console.log('connection:'.conn, 'failed:'.bad, self.endpoint);
        });
        this.client.on('game.ready', function(event) {
            console.log('status:'.action, 'ready');
        });
        this.client.on('game.waiting', function(event) {
            console.log('status:'.action, 'waiting');
        });
        this.client.on('game.start', function(event) {
            console.log('start:'.action, event.table);
        });
        this.client.on('game.start.failed', function(event) {
            console.log('start:'.action, 'failed:', event.table, event.message.dark);
        });
        this.client.on('game.score', function(event) {
            console.log('score:'.action, event.table, event.side);
            console.log(event.data.side[event.side]);
        });
        this.client.on('game.checkin', function(event) {
            console.log('checkin:'.action, (event.side + ':').dark, event.status, event.id);
            console.log(event.data);
        });
        this.client.on('game.scenario', function(event) {
            console.log('scenario:'.action, event.scenario);
        });
        this.client.on('game.complete', function(event) {
            console.log('complete:'.action, event.scenario);
        });
    };

    this.initialize();
}

require('util').inherits(FoosballClient, require('events').EventEmitter);

module.exports = new FoosballClient();