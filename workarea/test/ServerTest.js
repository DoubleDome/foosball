var UUID = require('node-uuid');
var colors = require('colors');
var prompt = require('prompt');
prompt.message = 'test'.green;

function ServerTest() {
    /* Variables
    --------------------------------------------------- */
    var _self = this;
    this.config = require('../server/config.json');
    this.commands = {
        'stats': 's',
        'update competitor': '+c',
        'all competitors': '*c',
    };

    /* Basic Functions
    --------------------------------------------------- */
    this.initialize = function() {
        colors.setTheme(this.config.theme);
        console.log('initialize:'.status, 'test'.dark);

        this.server = require('../server/FoosballServer');
        this.server.on('server.ready', this.startPrompt);
    };

    this.startPrompt = function() {
        if (!prompt.started) prompt.start();
        if (!prompt.running) {
            prompt.running = true;
            prompt.get(['call'], function(error, input) {
                prompt.running = false;
                if (typeof(input) === 'undefined' || typeof(input.call) === 'undefined') return;
                switch (input.call.toLowerCase()) {
                    case 's':
                        _self._callStats();
                        break;
                    case '*c':
                        _self.server.database.models.competitor.all(function(result) {
                            console.log(result);
                            _self.startPrompt();
                        });
                        break;
                    case '+c':
                        _self.collectCompetidor();
                        break;
                    default:
                        console.log('test:'.good, 'failed:'.bad, 'unrecognized mode'.dark);
                        console.log('\nAvailable Commands'.underline);
                        for (var command in _self.commands) {
                            console.log((command + ':').dark, _self.commands[command]);
                        }
                        console.log('');
                        _self.startPrompt();
                        return;
                }
            });
        }
    };

    this._callStats = function() {
        prompt.get(['rfid'], function(error, input) {
            if (typeof(input) === 'undefined') return;
            _self.server.database.models.competitor.getStats(input, function(result) {
                console.log(result);
                _self.startPrompt();
            });
        });
    };

    this.collectCompetidor = function() {
        prompt.get(['rfid', 'firstname', 'lastname', 'nickname', 'color', 'email'], function(error, input) {
            if (typeof(input) === 'undefined') return;
            _self.server.database.models.competitor.change({rfid: input.rfid}, input, function(error, result) {
                if (result) {
                    console.log('update:'.db, 'successful'.good);
                } else {
                    console.log('update:'.db, 'failed'.bad);
                }
                _self.startPrompt();
            });
        });
    };

    this.initialize();
}

module.exports = new ServerTest();