var UUID = require('node-uuid');
var colors = require('colors');
var prompt = require('prompt');
prompt.message = 'test'.green;

function GameTest() {
    /* Variables
    --------------------------------------------------- */
    var self = this;
    this.config = require('./config.json');

    /* Basic Functions
    --------------------------------------------------- */
    this.initialize = function() {
        colors.setTheme(this.config.theme);
        this.interval = 1500;
        console.log('initialize:'.status, 'test'.dark);

        this.table = require('../table/FoosballTable');
        this.table.on('game.complete', this.startPrompt);
        this.table.on('table.ready', this.startPrompt);
    };

    this.startPrompt = function() {
        if (!prompt.started) prompt.start();
        if (!prompt.running) {
            prompt.running = true;
            prompt.get(['mode'], function(err, result) {
                prompt.running = false;
                if (typeof(result) !== 'undefined') {
                    switch (result.mode.charAt(0)) {
                        case '1':
                            self.testSinglesGame(result.mode.charAt(1));
                            break;
                        case '2':
                            self.testDoublesGame(result.mode.charAt(1));
                            break;
                        case '3':
                            self.testTriplesGame(result.mode.charAt(1));
                            break;
                        case 's':
                            self.testSingleCheckin();
                            break;
                        case 'l':
                            self.table.serial.listPorts(function() {
                                self.startPrompt();
                            });
                            break;
                        case 'a':
                            self.table.audio.play('floop.wav');
                            self.startPrompt();
                            break;
                        default :
                            console.log('test:'.good, 'failed:'.bad, 'unrecognized mode'.dark);
                            self.startPrompt();
                            return;
                    }
                }
            });
        }
    };

    this.bogusGame = function(scores, callback) {
        scores.forEach(function(side, index) {
            setTimeout(function() {
                self.table.score(side);
            }, self.getDelay(index));
        });
        if (callback) setTimeout(callback, this.getDelay(scores.length));
    };
    this.bogusCheckin = function(players, callback) {
        players.forEach(function(player, index) {
            setTimeout(function() {
                self.table.checkin(player.side, player.rfid);
            }, self.getDelay(index));
        });
        if (callback) setTimeout(callback, this.getDelay(players.length));
    };
    /* Player Checkin
    --------------------------------------------------- */
    this.testSingleCheckin = function(type) {
        self.bogusCheckin([{
            side: 'one',
            rfid: 'D5C776E480'
        }], function() {
            self.table.start();
        });
    };
    this.testSinglesGame = function(type) {
        self.bogusCheckin([{
            side: 'one',
            // rfid: 'D5C776E480'
            rfid: '1234'
        }, {
            side: 'two',
            // rfid: '54EÏBF9BBFD'
            rfid: '4321'
        }], function() {
            self.table.start();
            self.runGame(type);
        });
    };
    this.testDoublesGame = function(type) {
        self.bogusCheckin([{
            side: 'one',
            rfid: '54EBF9BBFD'
        }, {
            side: 'two',
            rfid: 'D5C776E480'
        }, {
            side: 'one',
            rfid: UUID.v4()
        }, {
            side: 'two',
            rfid: UUID.v4()
        }], function() {
            self.table.start();
            self.runGame(type);
        });
    };
    this.testTriplesGame = function(type) {
        self.bogusCheckin([{
            side: 'one',
            rfid: UUID.v4()
        },{
            side: 'one',
            rfid: UUID.v4()
        }, {
            side: 'two',
            rfid: UUID.v4()
        },{
            side: 'two',
            rfid: UUID.v4()
        }, {
            side: 'one',
            rfid: UUID.v4()
        }, {
            side: 'two',
            rfid: UUID.v4()
        }], function() {
            self.table.start();
            self.runGame(type);
        });
    };
    this.runGame = function(type) {
        switch (type) {
            // regular game
            case 'r':
                this.bogusGame(['one', 'two', 'one', 'one']);
                break;
            // shutout
            case 's':
                this.bogusGame(['two', 'two', 'two']);
                break;
            // excess scoring
            case 'e':
                this.bogusGame(['two', 'two', 'two', 'two', 'two']);
                break;
            // neck to neck
            case 'n':
                this.bogusGame(['one', 'one', 'two', 'two', 'two']);
                break;
            default:
                console.log('test:'.good, 'failed:'.bad, 'unrecognized mode');
                self.startPrompt();
                break;
        }
    };
    this.getDelay = function(index) {
        return self.interval * (index + 1);
    };

    this.initialize();
}

module.exports = new GameTest();
