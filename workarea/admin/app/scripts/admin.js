admin.controller('LocationController', function($scope, $http, $filter, location) {
    $scope.map = {
        center: {
            latitude: 45,
            longitude: -73
        },
        zoom: 8
    };
    $scope.reset = function() {
        $scope.title = 'Create Location';
        $scope.data = {};
        $scope._queryLocations();
    };
    $scope.submit = function(valid) {
        valid = true;
        if (valid) {
            if (typeof($scope.data._id) === undefined) {
                location.save($scope.data).$promise
                    .then(function(result) {
                        $scope.reset();
                    }, function(error) {
                        console.log(error);
                    });
            } else {
                location.update($scope.data).$promise
                    .then(function(result) {
                        $scope.reset();
                    }, function(error) {
                        console.log(error);
                    });
            }
        } else {
            console.log('failed');
        }
    };
    $scope.edit = function(id) {
        $scope.title = 'Edit Location';
        location.get({
            id: id
        }).$promise
            .then(function(result) {
                $scope.data = result;
            }, function(error) {
                console.log(error);
            });
    };
    $scope.remove = function(id) {
        location.remove({
            id: id
        }).$promise
            .then(function(result) {
                $scope._queryLocations();
            }, function(error) {
                console.log(error);
            });
    };

    $scope._queryLocations = function() {
        location.query().$promise
            .then(function(result) {
                $scope.records = result;
            }, function(error) {
                console.log(error);
            });
    };

    $scope.reset();
});

admin.controller('TableController', function($scope, $http, $filter, table, location) {
    $scope.reset = function() {
        $scope.title = 'Create Table';
        $scope.data = {};
        $scope._queryTables();
        $scope._queryLocations();
    };
    $scope.submit = function(valid) {
        valid = true;
        if (valid) {
            $scope.data.location = $scope.data.location._id.toString();
            if (typeof($scope.data._id) === undefined) {
                table.save($scope.data).$promise
                    .then(function(result) {
                        $scope.reset();
                    }, function(error) {
                        console.log(error);
                    });
            } else {
                table.update($scope.data).$promise
                    .then(function(result) {
                        $scope.reset();
                    }, function(error) {
                        console.log(error);
                    });
            }
        } else {
            console.log('failed');
        }
    };
    $scope.edit = function(id) {
        $scope.title = 'Edit Table';
        table.get({
            id: id
        }).$promise
            .then(function(result) {
                $scope.data = result;
                var index;
                if (result.location) {
                    angular.forEach($scope.locations, function(value, key) {
                        if (value._id === result.location._id) index = key;
                    });
                    $scope.data.location = $scope.locations[index];
                }
            }, function(error) {
                console.log(error);
            });
    };
    $scope.remove = function(id) {
        table.remove({
            id: id
        }).$promise
            .then(function(result) {
                $scope._queryTables();
            }, function(error) {
                console.log(error);
            });
    };

    $scope._queryLocations = function() {
        location.query().$promise
            .then(function(result) {
                $scope.locations = result;
                // $scope.data.location = $scope.locations[0];
            }, function(error) {
                console.log(error);
            });
    };

    $scope._queryTables = function() {
        table.query().$promise
            .then(function(result) {
                $scope.records = result;
            }, function(error) {
                console.log(error);
            });
    };

    $scope.reset();
});

admin.controller('CompetitorController', function($scope, $http, $filter, competitor, location) {
    $scope.reset = function() {
        $scope.title = 'Create Competitor';
        $scope.data = {};
        $scope._queryCompetitors();
        $scope._queryLocations();
    };
    $scope.submit = function(valid) {
        valid = true;
        if (valid) {
            $scope.data.location = $scope.data.location._id.toString();
            if (typeof($scope.data._id) === undefined) {
                competitor.save($scope.data).$promise
                    .then(function(result) {
                        $scope.reset();
                    }, function(error) {
                        console.log(error);
                    });
            } else {
                competitor.update($scope.data).$promise
                    .then(function(result) {
                        $scope.reset();
                    }, function(error) {
                        console.log(error);
                    });
            }
        } else {
            console.log('failed');
        }
    };
    $scope.edit = function(id) {
        $scope.title = 'Edit Competitor';
        competitor.get({
            id: id
        }).$promise
            .then(function(result) {
                $scope.data = result;
                var index;
                if (result.location) {
                    angular.forEach($scope.locations, function(value, key) {
                        if (value._id == result.location._id) index = key;
                    });
                    $scope.data.location = $scope.locations[index];
                }
            }, function(error) {
                console.log(error);
            });
    };
    $scope.remove = function(id) {
        competitor.remove({
            id: id
        }).$promise
            .then(function(result) {
                $scope._queryCompetitors();
            }, function(error) {
                console.log(error);
            });
    };

    $scope._queryLocations = function() {
        location.query().$promise
            .then(function(result) {
                $scope.locations = result;
                // $scope.data.location = $scope.locations[0];
            }, function(error) {
                console.log(error);
            });
    };

    $scope._queryCompetitors = function() {
        competitor.query().$promise
            .then(function(result) {
                $scope.records = result;
            }, function(error) {
                console.log(error);
            });
    };

    $scope.reset();
});