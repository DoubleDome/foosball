var admin = angular.module('admin', ['ngResource', 'ngRoute', 'google-maps'])
    .factory('table', function($resource) {
        return $resource('/api/tables/:id', {
            _id: '@id'
        }, {
            update: {
                method: 'POST',
                params:{
                    id: "@_id"
                }
            }
        });
    })
    .factory('location', function($resource) {
        return $resource('/api/locations/:id', {
            _id: '@id'
        }, {
            update: {
                method: 'POST',
                params:{
                    id: "@_id"
                }
            }
        });
    })
    .factory('competitor', function($resource) {
        return $resource('/api/competitors/:id', {
            _id: '@id'
        }, {
            update: {
                method: 'POST',
                params:{
                    id: "@_id"
                }
            }
        });
    });

admin.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
            .when('/table', {
                templateUrl: '/partials/table.html',
                controller: 'TableController'
            })
            .when('/location', {
                templateUrl: '/partials/location.html',
                controller: 'LocationController'
            })
            .when('/competitor', {
                templateUrl: '/partials/competitor.html',
                controller: 'CompetitorController'
            })
            .otherwise({
                redirectTo: '/location'
            });
    }
]);