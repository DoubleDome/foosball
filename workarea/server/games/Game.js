function Game() {}
// Inherit prior to adding proto methods, wont work otherwise.
require('util').inherits(Game, require('events').EventEmitter);

Game.prototype.prepare = function(data) {
    this.running = false;
    // data is saved to the object in child game
    this.penultimatePoint = (this.data.maxGoals - 1);
    return this;
};
Game.prototype.reset = function() {
    this.data = undefined;
    return this;
};
Game.prototype.start = function() {
    if (!this.running) {
        this.running = true;
        this.data.start = new Date().getTime();
        this._broadcast('game.start');
    }
    return this;
};
Game.prototype.score = function(side) {
    if (this.running) {
        this.data.side[side].goals.push(new Date().getTime());
        this._broadcast('game.score', this.checkCombo(side));
        return this.checkScore();
    }
    return this;
};
Game.prototype.checkCombo = function(side) {
    var result = {side:side};
    switch (side) {
        case 'one':
            this.data.side.one.combo += 1;
            if (this.data.side.two.combo > 1) {
                result.breaker = true;
            }
            this.data.side.two.combo = 0;
            break;
        case 'two':
            this.data.side.two.combo += 1;
            if (this.data.side.one.combo > 1) {
                result.breaker = true;
            }
            this.data.side.one.combo = 0;
            break;
    }
    var target = this.data.side[side];
    if (target.combo > target.longestCombo) target.longestCombo = target.combo;
    if (target.combo > 1) result.combo = target.combo;
    return result;
};
// OVERRIDE
Game.prototype.checkScore = function() {
    var one = this.data.side.one;
    var two = this.data.side.two;

    // Game complete...
    if (one.goals.length >= this.data.maxGoals) {
        this.data.winner = one._id;
        this.data.loser = two._id;
        if (two.goals.length === 0) {
            return this._complete('shutout');
        } else {
            return this._complete();
        }
    }
    if (two.goals.length >= this.data.maxGoals) {
        this.data.winner = two._id;
        this.data.loser = one._id;
        if (one.goals.length === 0) {
            return this._complete('shutout');
        } else {
            return this._complete();
        }
    }
    return this.checkScenario();
};
Game.prototype.checkScenario = function(scenario) {
    var one = this.data.side['one'];
    var two = this.data.side['two'];
    // Special cases...
    if (two.goals.length == one.goals.length && two.goals.length == this.penultimatePoint) {
        this._broadcast('game.scenario', {
            scenario: 'neck2neck'
        });
    } else if (one.goals.length == this.penultimatePoint && two.goals.length === 0) {
        this._broadcast('game.scenario', {
            scenario: 'shutout'
        });
    } else if (two.goals.length == this.penultimatePoint && one.goals.length === 0) {
        this._broadcast('game.scenario', {
            scenario: 'shutout'
        });
    } else if (two.goals.length == this.penultimatePoint || one.goals.length == this.penultimatePoint) {
        this._broadcast('game.scenario', {
            scenario: 'about2win'
        });
    }
    return this;
};
// DO NOT OVERRIDE
Game.prototype._complete = function(scenario) {
    this.running = false;
    scenario = (scenario) ? scenario : 'regular';
    this.data.completeScenario = scenario;
    this.data.end = new Date().getTime();
    this.data.duration = this.data.end - this.data.start;

    this._broadcast('game.complete', {
        scenario: scenario
    });
    return this;
};
Game.prototype._broadcast = function(event, data) {
    if (!data) data = {};
    data.data = this.data;
    this.emit(event, data);
};

module.exports = Game;