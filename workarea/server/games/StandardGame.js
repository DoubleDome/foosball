var Game = require('./Game');

function StandardGame(data) {
    /* Variables
    --------------------------------------------------- */
    var self = this;
    this.data = data;

    /* Basic Functions
    --------------------------------------------------- */
    this.initialize = function() {
        console.log('StandardGame');
    };

    this.prepare(data);
}

require('util').inherits(StandardGame, Game);

module.exports = StandardGame;