var path = require('path');

function GameFactory() {
    /* Variables
    --------------------------------------------------- */
    var self = this;

    /* Basic Functions
    --------------------------------------------------- */
    this.initialize = function() {
        return this.load(path.resolve(__dirname, '../games'));
    };
    this.load = function(directory) {
        this.definitions = {};
        var fs = require('fs');
        var name;
        fs.readdirSync(directory).forEach(function(filename, index) {
            if (filename.indexOf('Game') !== 0 && filename.indexOf('.') !== 0) {
                name = filename.substring(0, filename.indexOf('Game')).toLowerCase();
                self.definitions[name] = require(path.join(directory, filename));
            }
        });
        return this;
    };
    this.create = function(data) {
        console.log(data);
        if (typeof(this.definitions[data.type]) !== 'undefined') {
            return new this.definitions[data.type](data);
        } else {
            throw new Error('GameFactory : Definition not found.');
        }
    };

    this.initialize();
}

module.exports = new GameFactory();