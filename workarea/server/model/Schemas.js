var mongoose = require('mongoose');

module.exports = {
    Location: {
        schema: {
            name: String,
            address: String,
            city: String,
            state: String,
            zip: Number,
            lat: Number,
            long: Number
        }
    },
    Table: {
        special: ['location'],
        schema: {
            location: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Location'
            },
            name: String,
            displayName: String,
            model: String
        }
    },
    Competitor: {
        special: ['location'],
        schema: {
            location: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Location'
            },
            firstname: String,
            lastname: String,
            nickname: String,
            rfid: String,
            color: String,
            email: String,
            players: [{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Competitor'
            }]
        }
    },
    Game: {
        special: ['location', 'table', 'players'],
        schema: {
            location: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Location'
            },
            table: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Table'
            },
            type: {
                type: String
            },
            side: {
                one: {
                    _id: {
                        type: mongoose.Schema.Types.ObjectId,
                        ref: 'Competitor'
                    },
                    players: [{
                        type: mongoose.Schema.Types.ObjectId,
                        ref: 'Competitor'
                    }],
                    goals: [Number]
                },
                two: {
                    _id: {
                        type: mongoose.Schema.Types.ObjectId,
                        ref: 'Competitor'
                    },
                    players: [{
                        type: mongoose.Schema.Types.ObjectId,
                        ref: 'Competitor'
                    }],
                    goals: [Number]
                }
            },
            start: Number,
            end: Number,
            mode: String,
            maxGoals: Number,
            winner: String,
            loser: String,
            players: [{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Player'
            }],
            completeScenario: String
        }
    }
};