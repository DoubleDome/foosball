module.exports = function(special) {
    var _processCall = function(call, callback, options) {
        if (options) {
            if (options.populate) {
                special.forEach(function(value, index) {
                    call = call.populate(value);
                });
            }
            if (options.lean) call = call.lean();
        }
        return call.exec().then(callback, _errorHandler);
    };
    var _errorHandler = function(error) {
        console.log('database:'.db, 'call:'.dark, 'failed:'.bad, error);
    };
    return {
        create: function(data, callback) {
            var Record = this.model(this.modelName);
            var target = new Record(data);
            target.save(function(error, result) {
                if (error) {
                    console.log('database:'.db, 'call:'.dark, 'failed:'.bad, error);
                    return;
                }
                callback(result);
            });
        },
        all: function(callback, options) {
            return _processCall(this.model(this.modelName).find(), callback, options);
        },
        get: function(query, callback, options) {
            return _processCall(this.model(this.modelName).findOne(query), callback, options);
        },
        contains: function(values, field, callback) {
            return _processCall(this.model(this.modelName).where(field).in(values), callback, options);
        },
        search: function(query, callback, options) {
            return _processCall(this.model(this.modelName).find(query), callback, options);
        },
        change: function(query, values, callback, options) {
            if (typeof(options) === 'undefined') options = {};
            return this.model(this.modelName).update(query, values, options, function(error, success, result) {
                if (error) {
                    console.log('database:'.db, 'call:'.dark, 'failed:'.bad, error);
                    return;
                }
                if (success) callback(result);
            });
        },
        destroy: function(query, callback) {
            return this.model(this.modelName).remove(query, function(error, success, result) {
                if (error) {
                    console.log('database:'.db, 'call:'.dark, 'failed:'.bad, error);
                    return;
                }
                if (success) callback(result);
            });
        }
    };
};