var mongoose = require('mongoose');

function CompetitorModel(definition) {
    /* Variables
    --------------------------------------------------- */
    var self = this;

    /* Basic Functions
    --------------------------------------------------- */
    this.getWithStats = function(query, callback) {
        this.get(query, function(competidor) {
            var id = competidor._id.toString();
            self.server.models.game.find({
                mode: 'singles',
                players: id
            }, function(games) {
                competidor.stats = self._prepareRecord(id, games);
                console.log(competidor);
                callback(competidor);
            });
        }, true);
    };

    this._prepareRecord = function(id, games) {
        var result = {
            longestCombo: 0
        };
        var wins = {
            longestStreak: 0,
            history: [],
            one: [],
            two: [],
            shutouts: []
        };
        var loses = {
            longestStreak: 0,
            history: [],
            one: [],
            two: [],
            shutouts: []
        };

        var winCount, lossCount;
        var targetObject, targetSide;

        games.forEach(function(game, index) {
            if (game.winner === id) {
                lossCount = 0;
                winCount += 1;
                if (winCount > wins.longestStreak) wins.longestStreak = winCount;
                targetObject = wins;
            } else {
                winCount = 0;
                lossCount += 1;
                if (lossCount > loses.longestStreak) loses.longestStreak = lossCount;
                targetObject = loses;
            }
            targetObject.history.push(game);
            targetSide = (game.side.one._id.toString() === id) ? 'one' : 'two';
            targetObject[targetSide].push(game);
            if (game.side[targetSide].longestCombo > result.longestCombo) {
                result.longestCombo = game.side[targetSide].longestCombo;
            }

            if (game.completeScenario === 'shutout') targetObject.shutouts.push(game);
        });

        wins.percentage = ((wins.history.length / games.length) * 100).toFixed(2);
        loses.percentage = ((loses.history.length / games.length) * 100).toFixed(2);

        result.wins = wins;
        result.loses = loses;

        console.log(id);
        console.log('count', 'wins:'.good, result.wins.history.length, 'loses:'.bad, result.loses.history.length);
        console.log('percentage', 'wins:'.good, result.wins.percentage, 'loses:'.bad, result.loses.percentage);
        console.log('streak', 'wins:'.good, result.wins.longestStreak, 'loses:'.bad, result.loses.longestStreak, 'combo:'.conn, result.longestCombo);
        console.log('one', 'wins:'.good, result.wins.one.length, 'loses:'.bad, result.loses.one.length);
        console.log('two', 'wins:'.good, result.wins.two.length, 'loses:'.bad, result.loses.two.length);
        console.log('shutouts', 'wins:'.good, result.wins.shutouts.length, 'loses:'.bad, result.loses.shutouts.length);
        return result;
    };

    this.initialize(definition);
}

require('util').inherits(CompetitorModel, require('./MongoModel'));

module.exports = CompetitorModel;