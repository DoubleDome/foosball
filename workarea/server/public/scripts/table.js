function TablePage() {
    /* Variables
    --------------------------------------------------- */
    var _self = this;


    /* Basic Functions
    --------------------------------------------------- */
    this.initialize = function() {
        this._applyActions();
    };

    this._applyActions = function() {
        $('#submit').click(this._submitHandler);
        $('#form').submit(this._submitHandler);
    };

    this._submitHandler = function(event) {
        console.log('submit');
        event.preventDefault();
        var data = {
            name:$('input[name="name"]').val(),
            displayName:$('input[name="displayName"]').val(),
            model:$('input[name="model"]').val(),
            location:$('select[name="location"]').val()
        };
        $.ajax({
            type: 'POST',
            url: '/table/create',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            crossDomain: true,
            dataType: 'json',
            success: function(data, status, jqXHR) {
                console.log(data);
                $('#form')[0].reset();
            },
            error: function(jqXHR, status) {
                // error handler
                console.log(jqXHR);
                console.log('fail' + status.code);
            }
        });
    };

    this.initialize();
}

$(window).load(function() {
    window.page = new TablePage();
});