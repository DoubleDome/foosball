admin.controller('TableController', function($scope, $http, $filter, table, location) {
    $scope.title = "Create Table";
    $scope.data = {};

    $scope.submit = function(valid) {
        valid = true;
        if (valid) {
            $scope.data.location = $scope.data.location._id.toString();
            if (typeof($scope.data._id) === undefined) {
                table.save($scope.data).$promise
                    .then(function(result) {
                        console.log(result);
                        $scope.data = {};
                        $scope._getTables();
                    }, function(error) {
                        console.log(error);
                    });
            } else {
                console.log($scope.data._id);
                table.update($scope.data).$promise
                    .then(function(result) {
                        console.log(result);
                        $scope.data = {};
                        $scope._getTables();
                    }, function(error) {
                        console.log(error);
                    });
            }
        } else {
            console.log('failed');
        }
    };
    $scope.editTable = function(id) {
        table.get({
            id: id
        }).$promise
            .then(function(result) {
                $scope.data = result;
                var index;
                if (result.location) {
                    angular.forEach($scope.locations, function(value, key) {
                        if (value._id === result.location._id) index = key;
                    });
                    $scope.data.location = $scope.locations[index];
                }
            }, function(error) {
                console.log(error);
            });
    };

    $scope.deleteTable = function(id) {
        table.delete({
            id: id
        }).$promise
            .then(function(result) {
                console.log(result);
                $scope._getTables();
            });
    };

    $scope._getLocations = function() {
        location.query().$promise
            .then(function(result) {
                $scope.locations = result;
                // $scope.data.location = $scope.locations[0];
            }, function(error) {
                console.log(error);
            });
    };

    $scope._getTables = function() {
        table.query().$promise
            .then(function(result) {
                $scope.records = result;
            }, function(error) {
                console.log(error);
            });
    };

    $scope._getLocations();
    $scope._getTables();
});