function LocationPage() {
    /* Variables
    --------------------------------------------------- */
    var _self = this;
    var siberia = new google.maps.LatLng(60, 105);
    var newyork = new google.maps.LatLng(40.69847032728747, -73.9514422416687);
    var current;

    /* Basic Functions
    --------------------------------------------------- */
    this.initialize = function() {
        this._getCurrentLocation(function(position) {
            _self.current = position;
            _self._createMap(_self.current);
            _self._getCurrentAddress(_self.current);
        });
        this._applyActions();
    };

    this._applyActions = function() {
        $('#submit').click(this._submitHandler);
        $('#update').click(this._updateHandler);
        $('#form').submit(this._submitHandler);
    };

    this._createMap = function(location) {
        var options = {
            zoom: 17,
            center: location,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };
        _self.map = new google.maps.Map(document.getElementById('map-canvas'), options);
        _self.marker = new google.maps.Marker({
            map: _self.map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: location
        });
    };

    this.find = function(address) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            address: address
        }, function(results, status) {
            switch (status) {
                case google.maps.GeocoderStatus.OK :
                    _self.marker.setPosition(results[0].geometry.location);
                    _self.map.setCenter(_self.marker.getPosition());
                    break;
                case google.maps.GeocoderStatus.ZERO_RESULTS :
                case google.maps.GeocoderStatus.OVER_QUERY_LIMIT :
                case google.maps.GeocoderStatus.REQUEST_DENIED :
                case google.maps.GeocoderStatus.INVALID_REQUEST :
                    console.log(status);
                    break;
            }
        });
    };

    this._getCurrentLocation = function(callback) {
        var browserSupportFlag;
        if (navigator.geolocation) {
            browserSupportFlag = true;
            navigator.geolocation.getCurrentPosition(function(position) {
                callback(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
            }, function() {
                callback(_self._geoLocationError(browserSupportFlag));
            });
        } else {
            browserSupportFlag = false;
            callback(_self._geoLocationError(browserSupportFlag));
        }
    };

    this._getCurrentAddress = function(location) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            location: location
        }, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                console.log(results);
            }
        });
    };



    this._geoLocationError = function(errorFlag) {
        if (errorFlag === true) {
            alert('Geolocation service failed.');
            return newyork;
        } else {
            alert('Your browser doesn\'t support geolocation. We\'ve placed you in Siberia.');
            return siberia;
        }
    };

    this._updateHandler = function(event) {
        event.preventDefault();
        console.log('update');
        _self.find($('textarea[name="address"]').val());
    };

    this._submitHandler = function(event) {
        console.log('submit');
        event.preventDefault();
        var data = {
            name:$('input[name="name"]').val(),
            address:$('textarea[name="address"]').val(),
            lat:_self.marker.position.lat(),
            lng:_self.marker.position.lng()
        };
        $.ajax({
            type: 'POST',
            url: '/location/create',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            crossDomain: true,
            dataType: 'json',
            success: function(data, status, jqXHR) {
                console.log(data);
            },
            error: function(jqXHR, status) {
                // error handler
                console.log(jqXHR);
                console.log('fail' + status.code);
            }
        });
    };

    this.initialize();
}

$(window).load(function() {
    window.page = new LocationPage();
});