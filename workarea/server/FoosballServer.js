var mongoose = require('mongoose');
var colors = require('colors');

function FoosballServer() {
    /* Variables
    --------------------------------------------------- */
    var self = this;
    this.config = require('./config.json');
    this.database = require('./controller/DatabaseController');

    /* Basic Functions
    --------------------------------------------------- */
    this.initialize = function() {
        colors.setTheme(this.config.theme);
        console.log('initialize:'.status, 'server'.dark);
        this.databaseEndpoint = this.config.database.host + ':' + this.config.database.port + '/' + this.config.database.name;

        this.database.connect(this.databaseEndpoint, function(error) {
            if (error) {
                console.log('connection:'.conn, 'failed:'.bad, self.databaseEndpoint);
                return;
            }
            self.createServer(self.config.server.port);
            self.createSockets(self.express.server);
            self.createGame();
            self.emit('server.ready');
        });
    };

    /* Server Setup
    --------------------------------------------------- */
    this.createServer = function(port) {
        this.express = require('./controller/ExpressController');
        this.express.initialize(port, this.database.models);
    };
    this.createSockets = function(server) {
        this.sockets = require('socket.io').listen(server, {
            log: true
        });
        this.sockets.on('connection', function(client) {
            console.log('connection:'.conn, 'open');
            client.on('table.register', function(data) {
                console.log('register:'.conn, 'table:'.dark, data.name);
                self.prepareTable(this);
                self.database.models.table.get({
                    name: data.name
                }, function(result) {
                    console.log(result);
                }, true);
                this.join(data.name);
            });
            client.on('client.register', function(data) {
                console.log('register:'.conn, 'client:'.dark, data.name);
                self.prepareClient(this);
                this.join(data.name);
            });
        });
    };
    /* Table/Client Preparation
    --------------------------------------------------- */
    this.prepareTable = function(table) {
        // query db for table record
        table.on('game.start', function(event) {
            self.game.createGame(event.name, {
                type: 'standard',
                maxGoals: 3
            }).start(event.name);
        });
        table.on('game.checkin', function(event) {
            self.game.checkin(event.name, event.side, event.rfid);
        });
        table.on('game.score', function(event) {
            self.game.score(event.name, event.side);
        });
    };
    this.prepareClient = function(client) {
        // You've got nothing to say right now...
    };
    /* Game Behavior
    --------------------------------------------------- */
    this.createGame = function() {
        this.game = require('./controller/GameController');
        this.game.on('game.ready', this._handleGameEvent);
        this.game.on('game.waiting', this._handleGameEvent);
        this.game.on('game.start', this._handleGameEvent);
        this.game.on('game.start.success', this._handleGameEvent);
        this.game.on('game.start.failed', this._handleGameEvent);
        this.game.on('game.score', this._handleGameEvent);
        this.game.on('game.checkin', this._handleGameEvent);
        this.game.on('game.scenario', this._handleGameEvent);
        this.game.on('game.complete', function(event) {
            self.database.models.game.save(event.data);
            self._handleGameEvent(event);
        });
        this.game.initialize(this.database);
    };

    this._handleGameEvent = function(event) {
        self.sockets.sockets.to(event.table).emit(event.type, event);
    };

    this.initialize();
}
require('util').inherits(FoosballServer, require('events').EventEmitter);


module.exports = new FoosballServer();