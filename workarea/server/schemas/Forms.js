var forms = require('forms');

function prepareLocations(locations) {
    var result = {};
    locations.forEach(function(location, index) {
        result[location._id.toString()] = location.name.toString();
    });
    return result;
}
module.exports = {
    competitor: function(locations) {
        locations = prepareLocations(locations);
        return {
            rfid: forms.fields.string({
                label: 'RFID',
                required: true
            }),
            firstname: forms.fields.string({
                label: 'First Name',
                required: true
            }),
            lastname: forms.fields.string({
                label: 'Last Name',
                required: true
            }),
            nickname: forms.fields.string({
                label: 'Nick Name'
            }),
            location: forms.fields.string({
                choices: locations,
                widget: forms.widgets.select(),
                required: true
            }),
            color: forms.fields.string({
                required: false,
                widget: forms.widgets.color()
            }),
            email: forms.fields.email()
        };
    },
    location: function() {
        return {
            name: forms.fields.string({
                required: true
            }),
            address: forms.fields.string({
                required: true,
                widget: forms.widgets.textarea()
            }),
            lat: forms.fields.number({
                widget: forms.widgets.hidden()
            }),
            lng: forms.fields.number({
                widget: forms.widgets.hidden()
            })
        };
    },
    table: function(locations) {
        locations = prepareLocations(locations);
        return {
            name: forms.fields.string({
                required: true
            }),
            displayName: forms.fields.string({
                label:'Display Name',
                required: true
            }),
            model: forms.fields.string({
                required: true
            }),
            location: forms.fields.string({
                choices: locations,
                widget: forms.widgets.select(),
                required: true
            }),
        };
    }
};