module.exports = function(mongoose, conn) {
    var result = {name:'Table'};
    result.special = ['location'];
    result.schema = mongoose.Schema({
        location: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Location'
        },
        name: {type: String},
        displayName: {type: String},
        model: {type: String}
    });

    result.schema.statics = require('../model/MongoModel')(result.special);
    result.model = mongoose.model(result.name, result.schema);

    return result;
};