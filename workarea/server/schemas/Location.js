module.exports = function(mongoose, conn){
    var result = {name:'Location'};
    result.special = [];
    result.schema = mongoose.Schema({
        name: {type:String},
        address: {type:String},
        lat: {type:Number},
        lng: {type:Number}
    });

    result.schema.statics = require('../model/MongoModel')(result.special);
    result.model = mongoose.model(result.name, result.schema);

    return result;
};
