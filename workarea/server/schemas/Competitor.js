module.exports = function(mongoose) {
    var result = {name: 'Competitor'};
    result.special = ['location'];
    result.schema = mongoose.Schema({
        location: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Location'
        },
        firstname: {type: String},
        lastname: {type: String},
        nickname: {type: String},
        rfid: {type: String},
        color: {type: String},
        email: {type: String},
        players: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Competitor'
        }]
    });
    /* Custom Methods
    --------------------------------------------------- */
    result.schema.statics = require('../model/MongoModel')(result.special);

    result.schema.statics.getStats = function(query, callback) {
        var _scope = this;
        if (typeof(query) === 'undefined') return;

        this.model(this.modelName).get(query, function(competitor) {
            if (competitor) {
                _scope._pullGameHistory(competitor, callback);
            } else {
                callback({error:true});
            }
        }, {lean:true});
    };
    result.schema.statics._pullGameHistory = function(competitor, callback) {
        var id = competitor._id.toString();
        var _scope = this;
        this.model('Game').search({
            mode: 'singles',
            players: id
        }, function(games) {
            competitor.stats = _scope._prepareRecord(id, games);
            callback(competitor);
        }, {lean:true});
    };
    result.schema.statics._prepareRecord = function(id, games) {
        var result = {
            longestCombo: 0
        };
        var wins = {
            longestStreak: 0,
            history: [],
            one: [],
            two: [],
            shutouts: []
        };
        var loses = {
            longestStreak: 0,
            history: [],
            one: [],
            two: [],
            shutouts: []
        };

        var winCount, lossCount;
        var targetObject, targetSide;

        games.forEach(function(game, index) {
            if (game.winner === id) {
                lossCount = 0;
                winCount += 1;
                if (winCount > wins.longestStreak) wins.longestStreak = winCount;
                targetObject = wins;
            } else {
                winCount = 0;
                lossCount += 1;
                if (lossCount > loses.longestStreak) loses.longestStreak = lossCount;
                targetObject = loses;
            }
            targetObject.history.push(game);
            targetSide = (game.side.one._id.toString() === id) ? 'one' : 'two';
            targetObject[targetSide].push(game);
            if (game.side[targetSide].longestCombo > result.longestCombo) {
                result.longestCombo = game.side[targetSide].longestCombo;
            }

            if (game.completeScenario === 'shutout') targetObject.shutouts.push(game);
        });

        wins.percentage = ((wins.history.length / games.length) * 100).toFixed(2);
        loses.percentage = ((loses.history.length / games.length) * 100).toFixed(2);

        result.wins = wins;
        result.loses = loses;

        // console.log(id);
        // console.log('count', 'wins:'.good, result.wins.history.length, 'loses:'.bad, result.loses.history.length);
        // console.log('percentage', 'wins:'.good, result.wins.percentage, 'loses:'.bad, result.loses.percentage);
        // console.log('streak', 'wins:'.good, result.wins.longestStreak, 'loses:'.bad, result.loses.longestStreak, 'combo:'.conn, result.longestCombo);
        // console.log('one', 'wins:'.good, result.wins.one.length, 'loses:'.bad, result.loses.one.length);
        // console.log('two', 'wins:'.good, result.wins.two.length, 'loses:'.bad, result.loses.two.length);
        // console.log('shutouts', 'wins:'.good, result.wins.shutouts.length, 'loses:'.bad, result.loses.shutouts.length);

        return result;
    };

    result.model = mongoose.model(result.name, result.schema);
    return result;
};