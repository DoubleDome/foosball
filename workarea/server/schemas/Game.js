module.exports = function(mongoose, conn){
	var result = {name:'Game'};
	result.special = ['location', 'table'];
	result.schema = mongoose.Schema({
		table: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Table'
		},
		type: {type: String},
		side: {
			one: {
				_id: {
					type: mongoose.Schema.Types.ObjectId,
					ref: 'Competitor'
				},
				players: [{
					type: mongoose.Schema.Types.ObjectId,
					ref: 'Competitor'
				}],
				goals: [{type: Number}],
				longestCombo: {type:Number}
			},
			two: {
				_id: {
					type: mongoose.Schema.Types.ObjectId,
					ref: 'Competitor'
				},
				players: [{
					type: mongoose.Schema.Types.ObjectId,
					ref: 'Competitor'
				}],
				goals: [{type: Number}],
				longestCombo: {type:Number}
			}
		},
		start: {type: Number},
		end: {type: Number},
		duration: {type: Number},
		mode: {type: String},
		maxGoals: {type: Number},
		winner: {type: String},
		loser: {type: String},
		players: [{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Player'
		}],
		completeScenario: {type: String}
	});
	/* Custom Methods
	--------------------------------------------------- */
    result.schema.statics = require('../model/MongoModel')(result.special);
    result.schema.statics.save = function(data) {
        // pull table id first
        var _self = this;
        _self.model('Table').get({
            name: data.table
        }, function(result) {
            data.table = result._id;
            _self.model('Game').create(data, function(result) {
                console.log('game:'.db, 'save:'.dark, 'successful:'.good, result);
            });
        });
    };

    result.model = mongoose.model(result.name, result.schema);
	return result;
};
