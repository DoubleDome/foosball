var mongoose = require('mongoose');
var MongoModel = require('../model/MongoModel');

function DatabaseController() {
    /* Variables
    --------------------------------------------------- */
    var _self = this;
    this.models = {};

    /* Basic Functions
    --------------------------------------------------- */
    this.connect = function(endpoint, callback) {
        mongoose.connection.on('error', function() {
            // _self.emit('error', arguments);
            // handle it
            callback('error');
        });
        mongoose.connection.once('open', function() {
            console.log('connection:'.conn, 'successful:'.good, endpoint);
            _self.createModels();
            callback();
        });
        mongoose.connect(endpoint);
    };
    this.createModels = function() {
        this.createModel('../schemas/Location');
        this.createModel('../schemas/Table');
        this.createModel('../schemas/Competitor');
        this.createModel('../schemas/Game');
    };
    this.createModel = function(path) {
        // var definition = require(path)(mongoose);
        // var model = new MongoModel(definition);
        var definition = require(path)(mongoose);
        this.models[definition.name.toLowerCase()] = definition.model;
    };

}

require('util').inherits(DatabaseController, require('events').EventEmitter);

module.exports = new DatabaseController();