var express = require('express');
var http = require('http');
var hbs = require('hbs');
var forms = require('forms');
var async = require('async');
var schemas = require('../schemas/Forms');
var _ = require('lodash');

function ExpressController() {
    /* Variables
    --------------------------------------------------- */
    var _self = this;

    /* Basic Functions
    --------------------------------------------------- */
    this.initialize = function(port, models) {
        this.models = models;
        this.express = this._createApp();
        this.server = http.createServer(this.express);
        this.server.listen(port);
        console.log('listening:'.conn, 'port:'.dark, port);
    };

    this._createApp = function() {
        var path = require('path');
        var app = express();
        app.use(require('body-parser').json());
        app.use(require('body-parser').urlencoded({
            extended: true
        }));
        app.set('view engine', 'hbs');
        // app.set('view engine', 'ejs');
        app.set('views', '../server/views');
        app.use(express.static('../server/public'));
        app.use('/admin', express.static('../admin/app'));
        app.use('/client', express.static('../client/dist'));

        // app.listen(port);
        // gets
        app.get('/', this._default);
        // app.get('/location/create', this._getLocationForm);
        // app.get('/table/create', this._getTableForm);
        // app.get('/competitor/:key/:value', this._getCompetitor);
        // app.get('/competitor/stats/:key/:value', this._getCompetitorStats);
        // app.get('/competitor/create', this._getCompetitorForm);

        // get
        app.get('/api/locations', this._getLocations);
        app.get('/api/tables', this._getTables);
        app.get('/api/competitors', this._getCompetitors);

        app.get('/api/locations/:id', this._getLocation);
        app.get('/api/tables/:id', this._getTable);
        app.get('/api/competitors/:id', this._getCompetitor);

        app.get('/api/competitors/:id/stats', this._getCompetitorStats);
        // posts
        app.post('/api/locations', this._postLocation);
        app.post('/api/tables', this._postTable);
        app.post('/api/competitors', this._postCompetitor);

        // puts
        app.post('/api/locations/:id', this._putLocation);
        app.post('/api/tables/:id', this._putTable);
        app.post('/api/competitors/:id', this._putCompetitor);

        // deletes
        app.delete('/api/locations/:id', this._deleteLocation);
        app.delete('/api/tables/:id', this._deleteTable);
        app.delete('/api/competitors/:id', this._deleteCompetitor);

        return app;
    };

    /* Handlers
    --------------------------------------------------- */
    this._default = function(request, response) {
        response.json({
            running: true
        });
    };
    /* Location
    --------------------------------------------------- */
    this._getLocations = function(request, response) {
        _self.models.location.all(function(result) {
            response.json(result);
        });
    };
    this._getLocation = function(request, response) {
        _self.models.location.get({
            _id: request.params.id
        }, function(result) {
            response.json(result);
        });
    };
    this._postLocation = function(request, response) {
        _self.models.location.create(request.body, function(result) {
            response.json({
                success: true
            });
        });
    };
    this._putLocation = function(request, response) {
        _self.models.location.change({
            _id: request.params.id
        }, request.body, function(result) {
            response.json({
                success: true
            });
        });
    };
    this._deleteLocation = function(request, response) {
        _self.models.location.destroy({
            _id: request.params.id
        }, function(result) {
            response.json({
                success: true
            });
        });
    };
    /* Table
    --------------------------------------------------- */
    this._getTables = function(request, response) {
        _self.models.table.all(function(result) {
            response.json(result);
        });
    };
    this._getTable = function(request, response) {
        _self.models.table.get({
            _id: request.params.id
        }, function(result) {
            response.json(result);
        }, {populate:true});
    };
    this._postTable = function(request, response) {
        _self.models.table.create(request.body, function(result) {
            response.json({
                success: true
            });
        });
    };
    this._putTable = function(request, response) {
        _self.models.table.change({
            _id: request.params.id
        }, request.body, function(result) {
            response.json({
                success: true
            });
        });
    };
    this._deleteTable = function(request, response) {
        _self.models.table.destroy({
            _id: request.params.id
        }, function(result) {
            response.json({
                success: true
            });
        });
    };
    /* Competitor
    --------------------------------------------------- */
    this._getCompetitors = function(request, response) {
        _self.models.competitor.all(function(result) {
            response.json(result);
        });
    };
    this._getCompetitor = function(request, response) {
        _self.models.competitor.get({
            _id: request.params.id
        }, function(result) {
            response.json(result);
        }, {populate:true});
    };

    this._postCompetitor = function(request, response) {
        _self.models.competitor.create(request.body, function(result) {
            response.json({
                success: true
            });
        });
    };
    this._putCompetitor = function(request, response) {
        _self.models.competitor.change({
            _id: request.params.id
        }, request.body, function(result) {
            response.json({
                success: true
            });
        });
    };
    this._deleteCompetitor = function(request, response) {
        _self.models.competitor.destroy({
            _id: request.params.id
        }, function(result) {
            response.json({
                success: true
            });
        });
    };
    /* Competitor Stats
    --------------------------------------------------- */
    this._getCompetitorStats = function(request, response) {
        _self.models.competitor.getStats({_id:request.params.id}, function(result) {
            response.json(result);
        });
    };

}

require('util').inherits(ExpressController, require('events').EventEmitter);

module.exports = new ExpressController();