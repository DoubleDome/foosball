var _ = require('lodash');
var LobbyController = require('./LobbyController');
var MongoModel = require('../model/MongoModel');

function GameController() {
    /* Variables
    --------------------------------------------------- */
    var _self = this;
    this.games = {};
    this.lobbies = {};
    this.factory = require('../factory/GameFactory');

    /* Basic Functions
    --------------------------------------------------- */
    this.initialize = function(database) {
        this.database = database;
    };
    this.reset = function(table) {
        if (this.hasGame(table)) {
            var game = this.getGame(table).reset();
            game.off('game.start');
            game.off('game.score');
            game.off('game.complete');
            game.off('game.scenario');
        }
        return this;
    };
    /* Creations
    --------------------------------------------------- */
    this.createLobby = function(table) {
        var lobby = this.lobbies[table] = new LobbyController(table, this.database);
        lobby.on('game.ready', function(event) {
            console.log('status:'.action, 'ready');
            _self._broadcast('game.ready', event);
        });
        lobby.on('game.waiting', function(event) {
            console.log('status:'.action, 'waiting');
            _self._broadcast('game.waiting', event);
        });
        lobby.on('game.checkin', function(event) {
            console.log('checkin:'.action, (event.side + ':').dark, 'complete');
            _self._broadcast('game.checkin', event);
        });
        return lobby;
    };
    this.createGame = function(table, data) {
        if (this.hasLobby(table)) {
            if (this.getLobby(table).state === 'ready') {
                console.log('create:'.action, table);
                data.table = table;
                data = _.merge(this.getLobby(table).data, data);

                this.clearLobby(table);
                var game = this.prepareGame(data, table);

                this.games[table] = game;
                _self._broadcast('game.start.success', {table:table, message:'Uneven number of players.'});
            } else {
                console.log('start:'.action, 'failed:'.bad, table, 'not ready'.dark);
                _self._broadcast('game.start.failed', {table:table, message:'Uneven number of players.'});
            }
        }
        return this;
    };
    this.prepareGame = function(data, table) {
        var game = this.factory.create(data);
        game.on('game.start', function(event) {
            console.log('start:'.action, table);
            event.table = table;
            _self._broadcast('game.start', event);
        });
        game.on('game.score', function(event) {
            console.log('score:'.action, event.side);
            event.table = table;
            _self._broadcast('game.score', event);
        });
        game.on('game.scenario', function(event) {
            console.log('scenario:'.action, event.scenario);
            event.table = table;
            _self._broadcast('game.scenario', event);
        });
        game.on('game.complete', function(event) {
            console.log('complete:'.action, event.scenario);
            event.table = table;
            _self._broadcast('game.complete', event);
        });
        return game;
    };
    /* Actions
    --------------------------------------------------- */
    this.checkin = function(table, side, playerId) {
        console.log('checkin:'.action, table, side, playerId);
        var lobby;
        if (this.hasLobby(table)) {
            lobby = this.getLobby(table);
        } else {
            lobby = this.createLobby(table);
        }
        lobby.checkin(side, playerId);
        return this;
    };
    this.start = function(table) {
        if (this.hasGame(table)) {
            this.getGame(table).start();
        }
    };

    this.score = function(table, side) {
        var game;
        if (this.hasGame(table)) {
            console.log('score:'.action, table, side);
            this.getGame(table).score(side);
        }
        return this;
    };

    /* Utility
    --------------------------------------------------- */
    this.getGame = function(table) {
        return this.games[table];
    };
    this.hasGame = function(table) {
        return (typeof(this.getGame(table)) !== 'undefined');
    };
    this.getLobby = function(table) {
        return this.lobbies[table];
    };
    this.hasLobby = function(table) {
        return (typeof(this.getLobby(table)) !== 'undefined');
    };
    this.clearLobby = function(table) {
        if (this.hasLobby(table)) {
            this.lobbies[table] = undefined;
        }
    };

    this._broadcast = function(type, data) {
        if (!data) data = {};
        data.type = type;
        this.emit(type, data);
    };
}

require('util').inherits(GameController, require('events').EventEmitter);

module.exports = new GameController();