var _ = require('lodash');

function LobbyController(table, database) {
    /* Variables
    --------------------------------------------------- */
    var _self = this;

    /* Basic Functions
    --------------------------------------------------- */
    this.create = function(table, database) {
        this.database = database;
        this.data = {
            table: table,
            mode: 'singles',
            side: {},
            players: []
        };
        this.one = this.data.side.one = {
            players: [],
            goals: [],
            combo: 0,
            longestCombo: 0
        };
        this.two = this.data.side.two = {
            players: [],
            goals: [],
            combo: 0,
            longestCombo: 0
        };
        this._broadcast('game.waiting', this.data);
        return this;
    };
    this.checkin = function(side, rfid) {
        if (this.data.side[side].players.length < 2) {
            console.log('checkin:'.db, 'rfid:'.dark, rfid);
            this._populatePlayer(side, rfid);
        } else {
            console.log('checkin:'.action, 'ignored:'.bad, (side + ':').dark, 'max players reached');
        }
        return this;
    };

    this._populatePlayer = function(side, rfid) {
        this.database.models.competitor.get({
            rfid: rfid
        }, function(competitor) {
            if (competitor) {
                console.log('checkin:'.db, 'player:'.dark, 'found:'.good, competitor._id);
                _self._store(side, competitor._id);
                _self._broadcast('game.checkin', {
                    id: competitor._id,
                    status: 'returning',
                    side: side,
                    lobby: _self.data,
                    data: competitor
                });
            } else {
                console.log('checkin failed');
                _self.database.models.competitor.create({
                    rfid: rfid
                }, function(result) {
                    console.log('checkin:'.db, 'player:'.dark, 'created:'.good, result._id);
                    _self._store(side, result._id);
                    _self._broadcast('game.checkin', {
                        id: result._id,
                        status: 'new',
                        side: side,
                        lobby: _self.data,
                        data: result
                    });
                });
            }
        });
    };
    this._populateTeam = function(side, target) {
        console.log(target.players);
        // needs reworking to pull team stats
        this.database.models.competitor.contains(target.players, 'players', function(result) {
            result = result[0];
            if (result) {
                console.log('checkin:'.db, 'team:'.dark, 'found:'.good, result._id);
                target._id = result._id;
                _self._broadcast('game.checkin', {
                    id: result._id,
                    status: 'returning',
                    side: side,
                    lobby: _self.data,
                    data: result
                });
            } else {
                _self.database.models.competitor.create({
                    players: target.players
                }, function(result) {
                    console.log('checkin:'.db, 'team:'.dark, 'created:'.good, result._id);
                    target._id = result._id;
                    _self._broadcast('game.checkin', {
                        id: result._id,
                        status: 'new',
                        side: side,
                        lobby: _self.data,
                        data: result
                    });
                    _self._checkCount();
                });
            }
        });
    };
    this._store = function(side, id) {
        this.data.players.push(id);

        var target = this.data.side[side];
        // Pseudo check for single/double
        if (target.players.length === 0) {
            target._id = id;
            target.players.push(id);
            this._checkCount();
        } else {
            delete target.rfid;
            this.data.mode = 'doubles';
            target.players.push(id);
            this._populateTeam(side, target);
        }
    };

    this._checkCount = function() {
        if (this.one.players.length === this.two.players.length && this.one.players.length > 0) {
            this._broadcast('game.ready', {
                data: this.data
            });
            this.state = 'ready';
        } else {
            this._broadcast('game.waiting', {
                data: this.data
            });
            this.state = 'waiting';
        }
        return this;
    };
    this._broadcast = function(type, data) {
        if (!data) data = {};
        data.table = this.data.table;
        this.emit(type, data);
    };

    this.create(table, database);
}

require('util').inherits(LobbyController, require('events').EventEmitter);

module.exports = LobbyController;