var exec = require('child_process').exec;
var path = require('path');
var fs = require('fs');

function AudioController() {
    /* Variables
    --------------------------------------------------- */
    var self = this;

    /* Basic Functions
    --------------------------------------------------- */
    this.initialize = function(library) {
        this.library = path.resolve(__dirname, '../', library);
    };

    this.play = function(name) {
        var audioPath = path.join(this.library, name);
        if (fs.existsSync(audioPath)) {
            exec('afplay ' + audioPath);
        } else {
            console.log('audio not found');
        }
    };


}

module.exports = new AudioController();