var prompt = require('prompt');
var serialport = require('serialport');
var _ = require('lodash');
prompt.message = 'table'.blue;
var fs = require('fs');
var path = require('path');

function ConfigController() {
    /* Variables
    --------------------------------------------------- */
    var self = this;

    /* Basic Functions
    --------------------------------------------------- */
    this.read = function(callback) {
        var data = require('../config.json');
        if (typeof(data.name) !== 'undefined') {
            callback(data);
        } else {
            prompt.start();
            self.promptForName(data, function(result) {
                console.log(data);
                data.name = result.name;
                data.displayName = result.displayName;
                data.serials = result.serials;
                self.write(path.resolve(__dirname, '../', 'config.json'), data, function(error) {
                    if (error) {
                        // write something here
                    }
                    console.log('table:'.blue, 'wrote config');
                    callback(data);
                });
            });
        }
    };

    this.write = function(location, data, callback) {
        fs.writeFile(location, JSON.stringify(data, null, 4), callback);
    };

    this.promptForName = function(data, callback) {
        prompt.get({
            properties: {
                name: {
                    description: 'Enter a name for the table',
                    type: 'string',
                    required: true
                }
            }
        }, function(error, result) {
            if (typeof(result) !== 'undefined') {
                data.displayName = result.name;
                data.name = data.displayName.toLowerCase().replace(/[^\w\d]/gi, '');
                data.serials = [];
                self.promptForSerial(data, callback);
            }
        });
    };
    this.promptForSerial = function(data, callback) {
        serialport.list(function(err, ports) {
            ports.forEach(function(port, index) {
                console.log((index + ':').yellow, port.manufacturer, port.pnpId, port.comName);
            });
            prompt.get({
                properties: {
                    index: {
                        description: 'Select USB device you wish to connect to',
                        type: 'number',
                        required: false
                    }
                }
            }, function(error, result) {
                if (typeof(result.index) === 'number' && result.index >= 0 && result.index < ports.length) {
                    data.serials.push(ports[result.index].comName);
                    self.promptForSerial(data, callback);
                } else {
                    callback(data);
                }
            });
        });
    };

    this.readOptions = function(nopt) {
        var known = {
            'init': String
        };
        var alias = {
            'i': 'init'
        };
        var result = nopt(known, alias);
        if (result.argv.remain.length > 0) {
            result.action = result.argv.remain.shift();

            switch (result.action) {
                case 'init':
                    console.log('init bitch'.bad);
                    break;
            }
        }
        return result;
    };
}

module.exports = new ConfigController();