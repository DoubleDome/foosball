var serialport = require('serialport');

function SerialController() {
    /* Variables
    --------------------------------------------------- */
    var self = this;
    this.clients = {};
    /* Basic Functions
    --------------------------------------------------- */
    this.create = function(address) {
        if (typeof(address) !== 'undefined') {
            var connection = new serialport.SerialPort(address, {
                parser: serialport.parsers.readline('\n'),
                baudrate: 9600
            }, false);
            this.clients[address] = connection;
            return connection;
        }
        return this;
    };
    this.connect = function(address) {
        var connection;
        if (!this.exists(address)) {
            connection = this.create(address);
        } else {
            connection = this.get(address);
        }
        connection.open(function(error) {
            if (error) {
                console.log('connection:'.conn, 'failed:'.bad, address);
                return;
            }
            console.log('connection:'.conn, 'successful:'.good, address);
            connection.on('data', function(data) {
                self.receive(data);
            });
            connection.on('error', function(error) {
                self.error(error);
            });
        });
        return this;
    };
    this.disconnect = function(address) {
        this.get(address).close(function(error) {
            if (!error) self.emit('serial.close', connection);
        });
        return this;
    };
    /* Transmission
    --------------------------------------------------- */
    this.send = function(message, address) {
        var connection;
        message += '\n';
        if (typeof(address) !== 'undefined') {
            this.get(address).write(message, this._writeHandler);
        } else {
            for (var client in this.clients) {
                this.get(client).write(message, this._writeHandler);
            }
        }
    };
    this.receive = function(data) {
        this.emit('serial.receive', data);
    };
    /* Utility
    --------------------------------------------------- */
    this.get = function(address) {
        return this.clients[address];
    };
    this.exists = function(address) {
        return (typeof(this.clients[address]) !== 'undefined');
    };
    /* Format
    --------------------------------------------------- */
    this.format = function(data) {
        return data;
    };
    /* Utility
    --------------------------------------------------- */
    this.listPorts = function(callback) {
        serialport.list(function(err, ports) {
            ports.forEach(function(port) {
                console.log(port.manufacturer, port.pnpId, port.comName);
            });
            callback();
        });
    };
    /* Event Handlers
    --------------------------------------------------- */
    this._writeHandler = function(error, result) {
        if (error) {
            console.log(error);
            return;
        }
    };
}

require('util').inherits(SerialController, require('events').EventEmitter);

module.exports = new SerialController();