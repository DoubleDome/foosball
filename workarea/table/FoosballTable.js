var io = require('socket.io-client');
var colors = require('colors');

function FoosballTable() {
    /* Variables
    --------------------------------------------------- */
    var _self = this;
    this.serial = require('./controller/SerialController');
    this.audio = require('./controller/AudioController');

    /* Basic Functions
    --------------------------------------------------- */
    this.initialize = function() {
        this.running = false;

        require('./controller/ConfigController').read(function(data) {
            _self.config = data;

            colors.setTheme(_self.config.theme);
            console.log('initialize:'.status, 'table:'.dark, _self.config.name);

            _self.endpoint = 'http://' + _self.config.server.host + ':' + _self.config.server.port;
            _self.connect(_self.config.name, _self.endpoint);

            _self.config.serials.forEach(function(address, index) {
                _self.serial.connect(address);
            });
        });

        this.serial.on('serial.receive', function(data) {
            _self.parseSerialCommand(data);
        });

        this.audio.initialize('./audio');
    };

    this.reset = function() {
        console.log('reset:'.dark, this.name);
        this.serial.send('reset');
        this.running = false;
    };
    this.connect = function(name, endpoint) {
        this.name = name;
        console.log('connecting:'.conn, endpoint);

        this.client = io.connect(endpoint);
        this.client.on('connect', function() {
            console.log('connection:'.conn, 'successful:'.good, endpoint);
            _self.broadcast('table.register', {
                name: name
            });
            _self.emit('table.ready');
        });
        this.client.on('disconnect', function() {
            console.log('connection:'.conn, 'disconnected:'.bad, endpoint);
            _self.reset();
        });
        this.client.on('connect_failed', function() {
            console.log('connection:'.conn, 'failed:'.bad, endpoint);
            _self.reset();
        });
        this.client.on('game.complete', function(event) {
            console.log('complete:'.action, event.scenario);
            _self.complete();
        });
        this.client.on('game.start.success', function(event) {
            _self.serial.send('start');
            console.log('start:'.action, 'success'.good);
        });
        this.client.on('game.start.failed', function(event) {
            console.log('start:'.action, 'failed'.bad);
        });
    };
    /* Game
    --------------------------------------------------- */
    this.start = function() {
        if (!_self.running) {
            console.log('start:'.action, this.name);
            _self.running = true;
            _self.broadcast('game.start');
            // send serial message to line breaker arduino
            // play audio
        } else {
            // play fail audio
        }
    };
    this.score = function(side) {
        if (this.running) {
            console.log('score:'.action, side);
            this.broadcast('game.score', {
                side: side
            });
            // play start audio
        } else {
            // play fail audio
        }
    };
    this.checkin = function(side, rfid) {
        if (!this.running) {
            console.log('checkin:'.action, side, rfid);
            this.broadcast('game.checkin', {
                side: side,
                rfid: rfid
            });
            // play checkin sound
        } else {
            // play fail audio
        }
    };
    this.complete = function() {
        this.reset();
        this.emit('game.complete');
        // play complete sound
    };
    /* Serial
    --------------------------------------------------- */
    this.parseSerialCommand = function(data) {
        // rfid|2|F1ACD41F0E00
        // goal|1
        var parts = data.split('|');
        switch (parts[0]) {
            case 'start':
                this.start();
                break;
            case 'reset':
                this.reset();
                break;
            case 'rfid':
                // 1: side, 2: rfid
                this.checkin(this.normalizeSide(parts[1]), parts[2]);
                break;
            case 'goal':
                this.score(this.normalizeSide(parts[1]));
                break;
        }
    };
    /* Util
    --------------------------------------------------- */
    this.normalizeSide = function(side) {
        switch (side) {
            case '1':
                return 'one';
            case '2':
                return 'two';
            default:
                return side;
        }
    };
    this.broadcast = function(event, data) {
        if (!data) data = {};
        data.name = this.name;
        this.client.emit(event, data);
    };

    this.initialize();
}

require('util').inherits(FoosballTable, require('events').EventEmitter);

module.exports = new FoosballTable();