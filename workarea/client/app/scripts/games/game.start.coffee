angular.module('foos').run ($rootScope) ->

  event = 'game.start'

  $rootScope.games[event] = (data, $scope) ->

    $scope.status = event

    $scope.data = data.data

    $rootScope.sound '/media/game.start.wav'

    console.log data


