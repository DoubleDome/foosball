angular.module('foos').run ($rootScope) ->

  event = 'competitor.checkin'

  $rootScope.games[event] = (data, $scope) ->

    $scope.status = event

    $scope.data = data.lobby

    $rootScope.sound '/media/game.checkin.mp3', .5
