'use strict'

###*
 # @ngdoc function
 # @name foos.controller:RoomCtrl
 # @description
 # # RoomCtrl
 # Controller of the foos
###

angular.module('foos')

  .controller 'RoomCtrl', ($rootScope, $scope, socket, $route, $filter, $routeParams) ->

    room = $route.current.params.room
    $scope.room = room
    $scope.data = {side: {one: {}, two: {}}}
    $scope.online = false

    $scope.urlParams = $routeParams

    # Socket Listener

    socket.on 'connect', (data) ->
      $scope.status = 'Connected to server'
      $scope.logs = data
      $scope.online = true

      socket.on 'disconnect', ->
        console.log 'Got disconnect!'
        $scope.online = false

      socket.emit('client.register', {
          name: room
      })


    gamesLoaded = []
    Object.keys($rootScope.games).forEach (game) ->
      gamesLoaded.push game
      socket.on game, (data) ->
        $rootScope.games[game](data, $scope, socket, room) if typeof $rootScope.games[game] == 'function'

    console.log "Loaded: #{gamesLoaded.join(', ')}"


    # Time in game
    $scope.timeInGame = ( since, now = new Date() ) ->
      return $filter('date')( (now - since), "m’s”")

    # Sound Player
    $rootScope.sound = (file, volume=1, forever=false) ->
      snd = new Audio file
      snd.volume = volume
      snd.preload = 'auto'
      snd.loop = true

# Get length of sounde
# prelaod
      if forever
        snd1 = new Audio file
        snd1.loop = true
        snd1.preload = 'auto'
        setTimeout ->
          snd1.play()
        , 4200
        snd1.loop = forever

      snd.play()



    # $rootScope.sound '/media/game.waiting.ogg', 1, true




