# <timer start="" end="">
#   <minutes></minutes>:<seconds></seconds>
# </timer>

angular.module('foos')
  .directive 'timer', ->
    return {

      restrict: 'E'
      template: '<minutes></minutes>:<seconds></seconds>'

      link: (scope, element, attrs) ->

        $minutes = $(element).find('minutes')
        $seconds = $(element).find('seconds')
        timer = false

        scope.$watch ->

          if attrs.start is ''
            $minutes.text("00")
            $seconds.text("00")
          else if attrs.end isnt ''
            clearInterval timer
            timer = false
          else if not timer
            timer = setInterval ->
              current = new Date( new Date() - attrs.start )
              minutes = ( 0 + current.getMinutes().toString() ).slice(-2)
              seconds = ( 0 + current.getSeconds().toString() ).slice(-2)
              $minutes.text(minutes)
              $seconds.text(seconds)
            ,1000
    }

  .directive 'minutes', ->
    return {
      restrict: 'E'
    }
  .directive 'seconds', ->
    return {
      restrict: 'E'
    }
