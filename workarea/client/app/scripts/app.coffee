'use strict'

SOCKET_IO = 'http://localhost:80' #'http://0.0.0.0:50001'

###*
 # @ngdoc overview
 # @name foos
 # @description
 # # foos
 #
 # Main module of the application.
###
angular

  # Modules
  .module('foos', [
    'ngRoute',
    'ngAnimate',
    # 3rd party dependencies
    'btford.socket-io'
  ])

  # Routes
  .config ($routeProvider) ->
    $routeProvider

      .when '/room/:room',
        templateUrl: 'views/room/room.html'
        controller: 'RoomCtrl'

      # .html5Mode(true)

  # Filters
  .filter 'range', ->
    return (val, range)  ->
      val = []
      i = 0
      range = parseInt(range)
      for [ 1..range ]
        val.push(i)
        i++
      return val


  # Services
  .factory 'socket', (socketFactory) ->

    myIoSocket = io.connect(SOCKET_IO)

    socket = socketFactory({
      ioSocket: myIoSocket
    })

    return socket



  .run ($rootScope) ->
    $rootScope.games = $rootScope.games or {}
